/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package fullstackjava;

import fullstackjava.GuiaPractica1.DesplegarEjerciciosUno;
import fullstackjava.GuiaPractica2.DesplegarEjercicioDos;
/**
 *
 * @author Hugo
 */
public class FullStackJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /*
        Para realizar esta guía de ejercicios utilicé clases de java las cuales importe en el archivo main
        Dejando descomentado solamente la importación de la clase del primer ejercicio y la instanciación de la mismma
        junto con el método saludo() para que al correr el programa no se ejecuten todos los ejercicios a la vez
        para ir probando los demás ejercicios solo se debe descomentar la importación de la clase al que pertenecen
        como asít las instaciación de la misma y de los métodos que ejecutan los ejercicios.
        */
        DesplegarEjerciciosUno des1 = new DesplegarEjerciciosUno();
        des1.desplegarEjGuia1();
        //DesplegarEjercicioDos des2 = new DesplegarEjercicioDos();
        //des2.desplegarEjGuia2();
        
        
    }
    
}
