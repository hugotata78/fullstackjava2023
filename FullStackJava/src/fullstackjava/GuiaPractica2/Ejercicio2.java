
package fullstackjava.GuiaPractica2;

import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Ejercicio2 {
    
    Scanner s = new Scanner(System.in);
    
    public void Palindromo(){
        System.out.print("Ingresa una palabra para saber si es un palindromo: ");
        String pal = this.s.nextLine().toLowerCase();
        boolean salir = false;
        for (int i = 0, j = pal.length() -1 ; i <= j && !salir; i++, j--){
            if(pal.charAt(i) != pal.charAt(j)){
                salir = true;
            }
        
    }
        System.out.println("La palabra "+pal+ (salir ? " no es un ": " es un ")+ "palindromo");
        
    }
}
