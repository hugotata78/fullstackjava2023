/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica2;

/**
 *
 * @author Hugo
 */
public class Ejercicio4 {
    
    int num;

    public Ejercicio4(int num) {
        this.num = num;
    }
    
    public void mostrarFactorial (){
        int fact = this.factorial(this.num);
        System.out.println("El factorial del numero "+this.num+" es: "+fact);
    }
    private int factorial(int num){
        if(num <= 0){
            return 1;
        }else{
            return num * this.factorial(num - 1);
        }
    }
    
}


