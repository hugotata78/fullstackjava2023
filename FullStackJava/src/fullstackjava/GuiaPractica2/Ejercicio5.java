/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica2;

/**
 *
 * @author Hugo
 */
public class Ejercicio5 {
    
    int serie;

    public Ejercicio5(int serie) {
        this.serie = serie;
    }
    
    public void seriefibonacci (){
        int num1 = 0, num2 = 1, suma = 1;
        System.out.println("Primeros "+this.serie+" numeros de la serie fibonacci: ");
        System.out.print(num1+", ");
        for(int i = 0; i < this.serie; i++){
            System.out.print(suma+", ");
            suma = num1 + num2;
            num1 = num2;
            num2 = suma;
             
        }
    }
    
}
