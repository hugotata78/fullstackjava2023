/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica2;

/**
 *
 * @author Hugo
 */
public class DesplegarEjercicioDos {
    
    Ejercicio1 ej1 = new Ejercicio1();
    Ejercicio2 ej2 = new Ejercicio2();
    Ejercicio3 ej3 = new Ejercicio3();
    Ejercicio4 ej4 = new Ejercicio4(6);
    Ejercicio5 ej5 = new Ejercicio5(10);
    Ejercicio6 ej6 = new Ejercicio6();
    Ejercicio7 ej7 = new Ejercicio7();
    
    public void desplegarEjGuia2(){
        //this.ej1.mostrarTablaDeMultiplicar();
        //this.ej2.Palindromo();
        //this.ej3.ordenarArray();
        //ej4.mostrarFactorial();
        //ej5.seriefibonacci();
        //ej6.mostrarPrimos();
        ej7.mostrarElegido();
        
    }
    
}
