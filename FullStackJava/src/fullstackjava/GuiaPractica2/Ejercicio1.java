
package fullstackjava.GuiaPractica2;

import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Ejercicio1 {
    
    Scanner s = new Scanner(System.in);
    
    public void mostrarTablaDeMultiplicar(){
        System.out.print("Ingrese el numero del que desea mostrar la tabla de multiplicar: ");
        int num = this.s.nextInt();
        System.out.println("Tabla de multiplicar del: "+num);
        for(int i = 0; i <= 10; i ++){
            System.out.println(num+" x "+i+" = "+num*i);
        }
    }
}
