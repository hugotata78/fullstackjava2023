/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica2;

/**
 *
 * @author Hugo
 */
public class Ejercicio3 {
    
    int arr[] = {7,3,9,2,1,5,8,4,6};
    
    public void ordenarArray(){
        
        System.out.println("Array desordenado: ");
        for(int i = 0; i < this.arr.length; i++){
            System.out.print(this.arr[i]+", ");
        }
        for (int i = 0; i < this.arr.length; i ++){
            for(int j = i+1; j < this.arr.length; j++){
                if(this.arr[i] < this.arr[j]){
                int num = this.arr[j];
                this.arr[j] = this.arr[i];
                this.arr[i] = num;
            }
            }
        }
        System.out.println("\nArray ordenado: ");
       for(int i = 0; i < this.arr.length; i++){
           System.out.print(this.arr[i]+", ");
       }
    }
    
}
