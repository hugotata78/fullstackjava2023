/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica2;

/**
 *
 * @author Hugo
 */
public class Animal {
    
    String nombre;
    boolean esHerviboro;
    boolean esDomesticable;
    boolean esMamifero;

    public Animal(String nombre, boolean esHerviboro, boolean esDomesticable, boolean esMamifero) {
        this.nombre = nombre;
        this.esHerviboro = esHerviboro;
        this.esDomesticable = esDomesticable;
        this.esMamifero = esMamifero;
    }
    
    public boolean esElegido(){
        return this.esHerviboro && this.esDomesticable && this.esMamifero;
    }
}
