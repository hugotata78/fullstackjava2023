/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica2;

/**
 *
 * @author Hugo
 */
public class Ejercicio6 {

    public void mostrarPrimos() {

        boolean esPrimo = false;
        System.out.println("Lista de numeros primos menores a 200");
        for (int i = 2; i <= 200; i++) {
 
            for (int j = 2; j < i && !esPrimo; j++) {
                if (i % j == 0) {
                    esPrimo = true;
                }
            }
            if(!esPrimo){
                System.out.println(i);
            }
            else
            {
                esPrimo = false;
            }
            
        }
    }
}
