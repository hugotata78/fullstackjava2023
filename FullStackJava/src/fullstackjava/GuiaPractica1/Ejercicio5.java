/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;
import java.lang.Math;
import java.util.Scanner;
/**
 *
 * @author Hugo
 */
public class Ejercicio5 {
    
    double area;
    double perimetro;
    double radio;
    Scanner s = new Scanner(System.in);
    
    public void calcularArea(){
        System.out.print("Ingrese el radio: ");
        this.radio = this.s.nextDouble();
        this.area = Math.PI * Math.pow(this.radio, 2);
        System.out.println("El area del círculo es: "+this.area);
    }
    
    public void calcularPerimetro(){
        System.out.print("Ingrese el radio: ");
        this.radio = this.s.nextDouble();
        this.perimetro = 2 * Math.PI * this.radio;
        System.out.println("El perimetro del círculo es: "+this.perimetro);
    }
}
