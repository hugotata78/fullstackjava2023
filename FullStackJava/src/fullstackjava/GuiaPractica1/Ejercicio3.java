/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;

import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Ejercicio3 {

    int num1;
    int num2;
    Scanner s = new Scanner(System.in);

    public int suma() {
        return this.num1 + this.num2;
    }

    public int resta() {
        return this.num1 - this.num2;
    }

    public int multiplicacion() {
        return this.num1 * this.num2;
    }

    public float division() {
        return this.num1 / this.num2;
    }

    public void calculadora() {
        int op;
        System.out.println("Ingrese el numero de la opcion deseada");
        System.out.println("1.- Suma");
        System.out.println("2.- Resta");
        System.out.println("3.- Multiplicacion");
        System.out.println("4.- Division");
        System.out.println("Seleccione la operacion que desea realizar: ");
        op = this.s.nextInt();

        switch (op) {
            case 1:
                System.out.println("Opcion seleccionada: Suma");
                System.out.print("Ingrese el primer numero: ");
                this.num1 = this.s.nextInt();
                System.out.print("Ingrese el segundo numero: ");
                this.num2 = this.s.nextInt();
                int resultadoSuma = this.suma();
                System.out.println("El resultado de la suma es: "+resultadoSuma);
                break;
            case 2:
                System.out.println("Opcion seleccionada: Resta");
                System.out.print("Ingrese el primer numero: ");
                this.num1 = this.s.nextInt();
                System.out.print("Ingrese el segundo numero: ");
                this.num2 = this.s.nextInt();
                int resultadoResta = this.resta();
                System.out.println("El resultado de la suma es: "+resultadoResta);
                break;
            case 3:
                System.out.println("Opcion seleccionada: Multiplicacion");
                System.out.print("Ingrese el primer numero: ");
                this.num1 = this.s.nextInt();
                System.out.print("Ingrese el segundo numero: ");
                this.num2 = this.s.nextInt();
                int resultadoMultiplicacion = this.multiplicacion();
                System.out.println("El resultado de la suma es: "+resultadoMultiplicacion);
                break;
            case 4:
                System.out.println("Opcion seleccionada: Division");
                System.out.print("Ingrese el primer numero: ");
                this.num1 = this.s.nextInt();
                System.out.print("Ingrese el segundo numero: ");
                this.num2 = this.s.nextInt();
                float resultadoDivision = this.division();
                System.out.println("El resultado de la suma es: "+resultadoDivision);
                break;
            default:
                System.out.println("La opcion seleccionada no es valida!");
        }

    }
}
