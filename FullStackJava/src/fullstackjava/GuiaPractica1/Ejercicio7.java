/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;

/**
 *
 * @author Hugo
 */
public class Ejercicio7 {
    
    int edad1;
    int edad2;

    public Ejercicio7(int edad1, int edad2) {
        this.edad1 = edad1;
        this.edad2 = edad2;
    }
    
    public void intercambiarEdades(){
        System.out.println("Las edades antes del intercambio son edad 1 "+this.edad1+" y edad 2 "+this.edad2);
        int edad3;
        edad3 = this.edad1;
        this.edad1 = this.edad2;
        this.edad2 = edad3;
        System.out.println("Las edades luego del intercambio son edad 1 "+this.edad1+" y edad 2 "+this.edad2);
    }
    
}
