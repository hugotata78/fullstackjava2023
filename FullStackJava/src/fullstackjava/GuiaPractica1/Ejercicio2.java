/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;

import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Ejercicio2 {
    
    public void saludo(){
        Scanner s = new Scanner(System.in);
        System.out.println("¿Como te llamas?");
        String name = s.next();
        System.out.println("Hola "+name);
    }
}
