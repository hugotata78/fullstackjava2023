/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;

/**
 *
 * @author Hugo
 */
public class Ejercicio4 {
    
    double est1;
    double est2;
    double est3;

    public Ejercicio4(double est1, double est2, double est3) {
        this.est1 = est1;
        this.est2 = est2;
        this.est3 = est3;
    }
    
    public double promedio (){
        return (this.est1 + this.est2 + this.est3) / 3;
    }
    
    
    
}
