/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;
import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Ejercicio6 {
    
    double precio;
    double porcentajeDescuento;
    double descuento;
    double precioConDescuento;
    Scanner s = new Scanner(System.in);
    
    public void calcularPrecioConDescuento(){
        System.out.print("Ingrese el precio del producto: ");
        this.precio = this.s.nextDouble();
        System.out.print("Ingrese el porcentaje del descuento: ");
        this.porcentajeDescuento = this.s.nextDouble();
        this.descuento = (this.precio * this.porcentajeDescuento) / 100;
        this.precioConDescuento = this.precio - this.descuento;
        System.out.println("El precio con descuento es: "+this.precioConDescuento);
        System.out.println("El descuento es: "+this.descuento);
    }
    
}
