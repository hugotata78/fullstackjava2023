/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;
import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Ejercicio9 {
    
    double pesos;
    double precioDolar = 231.68;
    double precioEuro = 250.69;
    double precioReal = 46.81;
    double precioPesosEnGuarani = 31;
    Scanner s = new Scanner(System.in);
    
    public void calcularCambio(){
        System.out.print("Ingrese la cantidad de pesos para calcular el precio en monedas extranjeras: ");
        this.pesos = this.s.nextDouble();
        System.out.printf("%.2f pesos equivalen a %.2f en dolares%n",this.pesos,this.pesos / this.precioDolar );
        System.out.printf("%.2f pesos equivalen a %.2f en euros%n", this.pesos,this.pesos / this.precioEuro );
        System.out.printf("%.2f pesos equivalen a %.2f en reales%n", this.pesos,this.pesos / this.precioReal);
        System.out.printf("%.2f pesos equivalen a %.2f en guaranies%n", this.pesos,this.pesos * this.precioPesosEnGuarani);
    }
    
}
