package fullstackjava.GuiaPractica1;

/**
 *
 * @author Hugo
 */
public class DesplegarEjerciciosUno {

    Ejercicio1 ej1 = new Ejercicio1();
    Ejercicio2 ej2 = new Ejercicio2();
    Ejercicio3 ej3 = new Ejercicio3();
    Ejercicio4 ej4 = new Ejercicio4(1.72, 1.67, 1.83);
    Ejercicio5 ej5 = new Ejercicio5();
    Ejercicio6 ej6 = new Ejercicio6();
    Ejercicio7 ej7 = new Ejercicio7(33, 28);
    Ejercicio8 ej8 = new Ejercicio8();
    Ejercicio9 ej9 = new Ejercicio9();

    public void desplegarEjGuia1() {
        this.ej1.saludo();
        //this.ej2.saludo();
        this.ej3.calculadora();
        //double promedio = this.ej4.promedio();
        //System.out.println("El promedio de estatura es: "+promedio);
        //this.ej5.calcularArea();
        //this.ej5.calcularPerimetro();
        //this.ej6.calcularPrecioConDescuento();
        //this.ej7.intercambiarEdades();
        //this.ej8.mostrarKelvinYFahrenheit();
        //this.ej9.calcularCambio();
    }
}
