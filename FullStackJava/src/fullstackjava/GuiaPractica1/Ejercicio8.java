/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fullstackjava.GuiaPractica1;

import java.util.Scanner;

/**
 *
 * @author Hugo
 */
public class Ejercicio8 {
    double celcius;
    double kelvin;
    double fahrenheit;
    Scanner s = new Scanner(System.in);
    
    public void mostrarKelvinYFahrenheit(){
        System.out.print("Ingrese los grados celcius que desea convertir: ");
        this.celcius = this.s.nextDouble();
        this.kelvin = 273.15 + this.celcius;
        this.fahrenheit = 1.8 * this.celcius;
        System.out.println(this.celcius+" grados celcius es equivalente a "+this.kelvin+" grados kelvin");
        System.out.println(this.celcius+" grados celcius es equivalente a "+this.fahrenheit+" grados fahrenheit");
    }
}
